from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from cdr.models import Cdr

# @login_required()
def index(request):
    cdr = Cdr.objects.all()
    return render(request, 'index.html', {'data': cdr})
