import datetime
from django.db import models
from django.utils import timezone


class Cdr(models.Model):
    connection_name = 'cdr'

    acctid = models.IntegerField(primary_key=True)
    calldate = models.CharField(max_length=80, null=True)
    clid = models.CharField(max_length=80, null=True)
    dcontext = models.CharField(max_length=80, null=True)
    channel = models.CharField(max_length=80, null=True)
    dstchannel = models.CharField(max_length=80, null=True)
    lastapp = models.CharField(max_length=80, null=True)
    lastdata = models.CharField(max_length=80, null=True)
    duration = models.IntegerField(null=True)
    billsec = models.IntegerField(null=True)
    disposition = models.CharField(max_length=45, null=True)
    amaflags = models.IntegerField(null=True)
    accountcode = models.CharField(max_length=20, null=True)
    uniqueid = models.CharField(max_length=150)
    userfield = models.CharField(max_length=255, null=True)
    record = models.CharField(max_length=255, null=True)
    src = models.CharField(max_length=255, null=True)
    dst = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.calldate + " " + str(self.src) + " → " + str(self.dst)

    def recent_calls(self):
        return datetime.strptime(self.calldate, '%Y-%m-%d %H-%M-%S') >= timezone.now() - datetime.timedelta(days=30)

    class Meta:
        db_table = 'cdr'
        app_label = 'cdr'

